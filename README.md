# The Fictional Meteorological Institute (FMI)
A group project for a networking course.

## Overview
1. [Intro](#intro)
2. [Running](#running)
    1. [Running Weather Station](#running-a-weather-station) 
    2. [Running Storage Server](#running-storage-server)
    3. [Running FMI CLI](#running-fmi-cli)
2.  [Project Overview](#project-overview)
3.  [Extras](#extras)
4.  [Testing](#testing)    
5.  [Authors](#authors)


## Intro
This is a simple implementation of a centralized server handling periodic streams of data from a number of weather stations, storing it in a cloud database and relaying data to clients upon request.

It is a demo and is not intended for use. Any use will require setting up a MongoDB account, database and provide login credentials in a `.env` file and make the appropriate changes to `storage_server.py`.


## Running

### weather_station.py
To run the weather station run:

```shell
$ python3 weather_station.py
```
You will then be asked to provide the following information:
1.  Location name:
2.  Interval:
3.  Host IP address:
4.  Port number:

### storage_server.py
To run the storage server run:

```shell
$ python3 storage_server.py
```

### fmi.py
To run the fmi client run:
```shell
$ python3 fmi.py
```


## Project Overview
![Project Diagram](resources/Overview.png)


## Extras
-   Multiple weather stations can run and transmit to the storage server.
-   Storing of weather data in a MongoDB cloud service.
-   Reading of stored weather data from a MongoDB cloud service.
-   Query storage server to get a list of all the locations in the database. 
-   Query storage server to receive all the weather data, or the data from a specified location.


## Testing
To enable debug mode set 
```python
DEBUG = True
```
in `fmi.py`, `storage_server.py`, or `weather_station.py` to see debug printouts to terminal. 


## Authors
-   Jacob Hagan
-   Vegard Haugland
-   Morten Kristiansen
-   Jonas Valen
