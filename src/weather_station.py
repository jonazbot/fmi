from station import StationSimulator
from weather_data import WeatherData
from socket import socket, AF_INET, SOCK_DGRAM
from time import sleep
import datetime
import pickle

STOP = False
DEBUG = False


class WeatherStation:

    def __init__(self, location: str, interval: int = 3600, server: str = 'localhost', port: int = 5555):
        self._loc = location
        self._repeat = interval
        self._address = (server, port)

    def start(self):
        global STOP
        global DEBUG
        if DEBUG:
            print(f'{self._loc} weather station reporting at {self._repeat} second intervals.')
        simulator = StationSimulator(simulation_interval=1)
        simulator.turn_on()
        udp_socket = socket(AF_INET, SOCK_DGRAM)
        while not STOP:
            try:
                sleep(self._repeat)
                dt = datetime.datetime.now()
                weather = WeatherData(date=dt.strftime('%Y.%m.%d'), time=dt.strftime('%H:%M:%S'), location=self._loc,
                                      temp=simulator.temperature, prec=simulator.rain)
                print(f'{weather.location} {weather.time}')
                udp_socket.sendto(pickle.dumps(weather), self._address)
            except Exception:
                print('Connection to server is currently unavailable.')
                STOP = True



if __name__ == '__main__':
    loc: str = str(input('Enter location name: '))
    repeat: int = int(input('Enter repeat interval in seconds: '))
    host: str = str(input('Enter server address (or leave empty for "localhost"): '))
    if host == '':
        host = 'localhost'  # For Windows...
    port_nr: int = int(input('Enter server port number: '))

    weatherStation = WeatherStation(location=loc, interval=repeat, server=host, port=port_nr)
    weatherStation.start()
