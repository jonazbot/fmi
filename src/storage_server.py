from socket import socket, AF_INET, SOCK_DGRAM
from multiprocessing import Process
from time import sleep
from weather_data import WeatherData
from dotenv import load_dotenv
import pymongo
import pickle
import os

STOP = False
DEBUG = False


class TCPConnection(Process):

    def __init__(self, host: str = 'localhost', port: int = 5555):
        Process.__init__(self)
        self._host = host
        self._port = port

    def run(self) -> None:
        global STOP
        global DEBUG
        tcp_socket = socket()
        tcp_socket.bind((self._host, self._port))
        tcp_socket.listen()

        db_client = pymongo.MongoClient(
            f'mongodb+srv://admin:{os.getenv("MONGO_PWD")}@cluster0.lsx9f.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
        database = db_client.weather
        table = database.data

        if DEBUG:
            print('[TCP] Started.')
        while not STOP:
            try:
                if DEBUG:
                    print('[TCP] Waiting for connection...')
                tcp_connection, (address, port) = tcp_socket.accept()
                if DEBUG:
                    print(f'[TCP] Connection established with {address}.')
                while not STOP:
                    if DEBUG:
                        print('[TCP] Waiting for query...')
                    try:
                        query = tcp_connection.recv(1024).decode()
                        if DEBUG:
                            print(f'[TCP] Query "{query}" received.')
                        if query == '':
                            break
                        elif query.lower() == 'loc':
                            locations = list(table.distinct('location'))
                            tcp_connection.send("|".join(locations).encode())
                            if DEBUG:
                                print("[TCP] All locations sent.")
                        elif query.lower() == 'all':
                            result = list(table.find())
                            tcp_connection.send(str(len(result)).encode())
                            if DEBUG:
                                print(f'[TCP] Sending all {len(result)} entries.')
                            self._send(result, tcp_connection)
                        elif query.lower().startswith('get'):
                            location = query.split(' ', 1)[1]
                            result = list(table.find({'location': location}))
                            tcp_connection.send(str(len(result)).encode())
                            if DEBUG:
                                print(f'[TCP] Sending all {len(result)} entries on {location}.')
                            self._send(result, tcp_connection)
                        else:
                            if DEBUG:
                                print(f'[TCP] Unknown query "{query}".')
                            continue
                    except Exception:
                        tcp_connection.close()
                        print(f'[TCP] Connection closed with {address}.')
                        break
            finally:
                print(f'[TCP] Client disconnected.')
        tcp_socket.close()

    def _send(self, results: list, tcp_con):
        if len(results) > 0:
            sent = 0
            for line in results:
                wd = WeatherData(date=line['date'], time=line['time'], location=line['location'],
                                 temp=line['temp'], prec=line['prec'])
                tcp_con.send(pickle.dumps(wd))
                sleep(0.01)
                sent += 1
                if DEBUG:
                    print(f'[TCP] Sending: {wd.location} {wd.time}.')
            if DEBUG:
                print(f'[TCP] Done. Sent {sent} entries.')


class UDPConnection(Process):

    def __init__(self, host: str = 'localhost', port: int = 5555):
        Process.__init__(self)
        self._host = host
        self._port = port

    def run(self) -> None:
        global STOP
        global DEBUG
        udp_socket = socket(AF_INET, SOCK_DGRAM)
        table = None
        try:
            udp_socket.bind((self._host, self._port))
            print('[UDP] Started.')
            db_client = pymongo.MongoClient(
                f'mongodb+srv://admin:{os.getenv("MONGO_PWD")}'
                f'@cluster0.lsx9f.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
            database = db_client.weather
            table = database.data
            if DEBUG:
                print('[UDP] Connected to database.')
        except Exception:
            print('[UDP] Error with establishing connection.')
            STOP = True
        while not STOP:
            try:
                if DEBUG:
                    print('[UDP] Ready to receive weather data...')
                wd = pickle.loads(udp_socket.recv(1024))
                if DEBUG:
                    print(f'[UDP] New data from {wd.location} {wd.time}.')
                table.insert_one(wd.dictionary())
            except Exception:
                print('[UDP] server shutting down...')
                STOP = True
        udp_socket.close()


if __name__ == '__main__':
    port: int = int(input('Enter port number: '))
    udp = UDPConnection('', port)
    udp.daemon = True
    tcp = TCPConnection('', port)
    tcp.daemon = True
    try:
        load_dotenv()
        tcp.start()
        udp.start()
    except Exception:
        print('Unexpected closure. Shutting down...')
        STOP = True
    while tcp.is_alive() and udp.is_alive():
        sleep(1)
    STOP = True
    tcp.terminate()
    udp.terminate()
    tcp.join()
    udp.join()
