from socket import socket
import pickle

DEBUG = False


class FMI:

    def __init__(self, server: str = 'localhost', port: int = 5555):
        self._server = server
        self.address = (server, port)
        self._tcp_client = socket()
        self._fmi_ascii = '\n ___ __  __ ___    ___ _    ___ \n' \
                          '| __|  \/  |_ _|  / __| |  |_ _| \n' \
                          '| _|| |\/| || |  | (__| |__ | |  \n' \
                          '|_| |_|  |_|___|  \___|____|___|\n'
        self._commands = 'Commands\n' \
                         'loc - Get all available locations in the database\n' \
                         'all - Get all weather data from the database\n' \
                         'get [location] - Get all weather data from the specified location\n' \
                         'exit - Exit FMI CLI\n' \
                         'help - Show available commands'

    def connect(self):
        self._tcp_client.connect(self.address)
        print('Connected!')
        print(self._fmi_ascii)
        print(f'{self._commands}')
        while True:
            query = input('\nEnter command: ')
            if query == 'exit':
                break
            self._command_handler(query)
        self._tcp_client.close()

    def _request_all(self):
        global DEBUG
        if DEBUG:
            print('Requesting all entries...')
        self._tcp_client.send('all'.encode())
        self._print_weather_data_response()

    def _print_weather_data_response(self):
        global DEBUG
        num_entries = int(self._tcp_client.recv(1024).decode())
        entries = []
        if DEBUG:
            print(f'Receiving {num_entries}. Please wait...')
        for i in range(num_entries):
            entries.append(pickle.loads(self._tcp_client.recv(1024)))
        for entry in entries:
            print(entry.stringify())
        if DEBUG:
            print(f'Received {len(entries)}')

    def _command_handler(self, command: str = '?'):
        if command.lower() == 'loc':
            self._print_locations()
        elif command.lower() == 'all':
            self._request_all()
        elif command.lower().startswith('get '):
            if len(command.split(' ')) > 1:
                self._request_by_location(command)
            else:
                print('Please specify a location after "get"')
        elif command.lower() == 'help' or command == '?':
            print(self._commands)
        else:
            print(f'Unknown command: {command}')

    def _request_by_location(self, command: str):
        self._tcp_client.send(command.encode())  # Send request to server
        self._print_weather_data_response()

    def _print_locations(self):
        self._tcp_client.send('loc'.encode())  # Send request to server
        msg = self._tcp_client.recv(1024).decode()  # Receive from server
        locations = msg.split('|')
        print('Available locations in database: ')
        loc = 0
        for location in locations:
            print(location.title(), end='')
            if loc == len(locations) - 1:
                print()
            else:
                print(', ', end='')
            loc += 1


if __name__ == '__main__':
    server: str = str(input('Enter server address (or leave empty for "localhost"): '))
    if server == '':
        server = 'localhost'  # For Windows...
    port: int = int(input('Enter server port number: '))
    fmi = FMI(server=server, port=port)
    try:
        print('Connecting to server...')
        fmi.connect()
    except Exception:
        print('Unable to connect to server. Please try again later.\nShutting down...')
