class WeatherData:

    def __init__(self, date: str, time: str, location: str, temp: int, prec: int):
        self.date = date
        self.time = time
        self.location = location
        self.temp = temp
        self.prec = prec

    def stringify(self):
        return f'{self.location:15} | {self.date} | {self.time} | Temp: {self.temp:6} | Precipitation: {self.prec}'

    def dictionary(self):
        return {
            'location': self.location,
            'date'    : self.date,
            'time'    : self.time,
            'temp'    : self.temp,
            'prec'    : self.prec
        }
